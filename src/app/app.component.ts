import { HomePage } from './../pages/home/home';
import { Component } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage'


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any 

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private storage: Storage,
    public modalCtrl: ModalController
  ){
    platform.ready().then(() => {
      console.log('executando verificacao')
      this.storage.get('user')
      .then((resolve) =>{
        if(resolve.length > 0){
          this.rootPage = 'start-page'
        }else {
          this.rootPage = HomePage
        }
      })
      .catch((error) =>{
        this.rootPage = HomePage
      })

      statusBar.backgroundColorByHexString('#222'); 

      let splashPage = modalCtrl.create("SplashPage");
      splashPage.present();
    });
  }
}

