import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading} from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from 'ionic-angular';
import { Title } from '@angular/platform-browser';
import { Storage } from '@ionic/storage'


@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loader: Loading

  loginForm: FormGroup

  showPassord = false;

  constructor(
  public navCtrl: NavController, 
  public navParams: NavParams,
  public formBuilder: FormBuilder,
  public afAuth: AngularFireAuth,
  public alertCtrl: AlertController,
  public storage: Storage,
  public loadingCtrl: LoadingController
  ) {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    })
  }
  submitLogin(){
    this.presentLoading()

    this.afAuth.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password)
    .then((response) =>{
      this.loader.dismiss()
      this.storage.set('user', response.user.uid )
      .then(() => {
      this.navCtrl.setRoot('start-page')
      })
    })
    .catch((error) =>{
      this.loader.dismiss()
      if(error.code == 'auth/wrong-password'){
        this.presentAlert('erro', 'Senha incorreta, Digite Novamente.');
        this.loginForm.controls['password'].setValue(null);
      }
    })

  }
  presentAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }  

  ionViewCanEnter () {
    this.storage.get('user')
    .then((resolve) =>{
      if(resolve.length > 0){
        this.navCtrl.setRoot('start-page');
      }else {
        return true; 
      }
    })
    .catch((error) =>{
      return true;
    })
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando...",
    });
    this.loader.present();
  }

}