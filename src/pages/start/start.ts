import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, List, LoadingController, Loading, ToastController, ModalController, AlertController, } from 'ionic-angular';
import { Storage } from '@ionic/storage'
import { AngularFireDatabase, listChanges } from '@angular/fire/database';
import { HomePage } from '../home/home';
import * as moment from 'moment';


@IonicPage({
  name: 'start-page'
})
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {
  load1: Loading
  uid: string;
  task;
  list_task_new = []
  list_task_finish = []


  task_seccion = "task_new"

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public db: AngularFireDatabase,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController
  ) {
    console.log(moment.locale('pt-br'));
    console.log(moment().format('l') + " " + moment().format('LTS'));
  }

  ionViewDidLoad() {
    this.load1 = this.loadingCtrl.create({
      content: 'carregado'
    })
    this.load1.present()
    this.storage.get('user')
      .then((resolve) => {
        this.uid = resolve;
        this.getlist()
      }).catch(() => {
        this.load1.dismiss()
        this.navCtrl.setRoot(HomePage)
      })
  }

  addTask(task: String) {
    this.db.database.ref('/tasks').child(this.uid).push({
      task: task,
      status: false,
      create_at: moment().format('l') + " " + moment().format('LTS')
    })
      .then(() => {
        this.task = "";
      })
  }

  getlist() {
    let self = this
    let listDB = this.db.database.ref('/tasks').child(this.uid);
    listDB.on('value', (snapshot) => {
      if (snapshot.exists()) {
        this.list_task_new = []
        this.list_task_finish = []
        Object.keys(snapshot.val()).map((key) => {
          let item = snapshot.val()[key]
          console.log(item.task)
          let task = {
            uid: key,
            name: item.task,
            status: item.status,
            create_at: item.create_at
          }

          if (item.status == false) {
            this.list_task_new.push(task)
          } else {
            this.list_task_finish.push(task)
          }

        })
        self.load1.dismiss()
        console.log('this.list', this.list_task_new)
      } else {
        this.list_task_new = []
        this.list_task_finish = []
        this.load1.dismiss()
      }
    })

  }

  logout() {
    this.storage.remove('user')
    this.navCtrl.setRoot(HomePage)
  }

  delete(uid) {
    console.log('delete()', uid)
    this.db.database.ref('/tasks').child(this.uid).child(uid).remove()

  }

  updateStatus(item) {
    let self = this;
    let status = !item.status
    setTimeout(function () {
      self.db.database.ref('/tasks').child(self.uid).child(item.uid).update({
        status: status
      }).then(() => {
        self.presentToast('Sua tarefa foi movida para lista de tarefas concluidas')
      })
    }, 1000);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom',
    });

    toast.present();
  }

  pushModal(task) {
    let profileModal = this.modalCtrl.create('TaskDetailPage', { task: task });
    profileModal.present();

    profileModal.onDidDismiss(data => {
      console.log(data);
    });
  }

  confirmPassword() {
    let alert = this.alertCtrl.create({
      title: 'Nova tarefa',
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            console.log(data)
            this.addTask(data.nome)
          }
        }
      ]
    });
    alert.present();
  }

  alertTaskDetail(task) {

    let datetime = task.create_at.split(' ', 2)
    let alert = this.alertCtrl.create({
      title: '<h1 class="c-no-margin winner-alert-title">Detalhe da tarefa</h1>',
      message: `
      
      <ul>
          <li>Nome: `+ task.name + `</li>
          <li>Status: ` + task.status + `</li>
          <li>Data: `+ datetime[0] + ` </li>
          <li>Hora: `+ datetime[1] + `</li>
        
      </ul> 
      `,
    });
    alert.present();
  }




}
