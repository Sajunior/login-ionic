import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';


@IonicPage({
  name: 'create-user'
})
@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
})
export class CreateUserPage {

  loader: Loading;

  registerForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public NavParams: NavParams,
    public formbuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ){

    let ValidateConfirmPassword = (control: FormControl) => {
      if( control.value !== '' ){
        if(control.value === this.registerForm.controls.password.value){
          console.log(control.value === this.registerForm.controls.password.value)
          return null
        } else {
          console.log('invalid')
          return { "invalid": true }
        }
      }
    }
    
    this.registerForm = this.formbuilder.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6), ValidateConfirmPassword]]
    })
  }
  submitForm(){
    this.presentLoading()

  this.afAuth.auth.createUserWithEmailAndPassword(
    this.registerForm.value.email, this.registerForm.value.password)
    .then ((Response) => {
      this.loader.dismiss()
      this.presentAlert('Usuario Cadastrado', 'Usuario Cadastrado com suecesso');
      this.navCtrl.setRoot(HomePage)
    })
    .catch((error) => {
      this.loader.dismiss()
      if(error.code == 'auth/email-already-in-use'){
        this.presentAlert('error', 'Email ja Cadastrado')
      }
    })
  }
  presentAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  } 
  
  
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando...",
      
    });
    this.loader.present();
  }
}